# Deployment

kubectl apply -f tagger-namespace.yaml

kubectl apply -f mysql-pvc.yaml -f mysql-deployment.yaml -f mysql-svc.yaml

# Ingress

To get the ingress working create a pair of self-signed certificates create your tls secret and remember to edit ingress with your own fqdn

```bash
mkdir certs
openssl req -x509 -nodes -addext "subjectAltName = DNS:tagger.ak8s.net" -newkey rsa:2048 -keyout certs/tagger.ak8s.net.key -out certs/tagger.ak8s.net.crt
kubectl -n tagger create secret tls tagger-tls --cert=certs/tagger.ak8s.net.crt --key=certs/tagger.ak8s.net.key --dry-run=client -o yaml > tagger-tls.yaml
kubectl apply -f tagger-tls.yaml
```

kubectl apply -f tagger-clusterrole.yaml -f tagger-clusterrolebinding.yaml -f tagger-serviceaccount.yaml -f tagger-deployment.yaml -f tagger-svc.yaml -f tagger-ingress.yaml

# To run migrations

kubectl exec -it <tagger-pod> -c tagger-fe -- php artisan migrate --seed --seeder=AdminCoreSeeder
